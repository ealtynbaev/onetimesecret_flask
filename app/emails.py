from flask.ext.mail import Message
from app import app, mail
from config import ADMINS
from flask import render_template
from threading import Thread

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender = sender, recipients = recipients)
    msg.body = text_body
    msg.html = html_body
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()

def notification(email, link):
    send_email("Secret on DTLN onclick!",
        ADMINS[1],
        [email],
        render_template("email.txt", 
            link = link),
        render_template("email.html", 
            link = link))