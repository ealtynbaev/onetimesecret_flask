#!/usr/bin/env python
# -*- coding: utf-8 -*-


from datetime import datetime
from hashlib import md5
from flask import url_for
from app import db, lm
import uuid
from app.cryptograph import AESCipher, pad, unpad
import config

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(120), index = True, unique = True)
    posts = db.relationship('SecretInst', backref = 'author', lazy = 'dynamic')
    last_seen = db.Column(db.DateTime)


    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + '?d=mm&s=' + str(size)


    def __repr__(self):
        return self.email




class SecretInst(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    secret_f = db.Column(db.String(64), index = True, unique = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    email = db.Column(db.String(120), index = True)
    viewed = db.Column(db.DateTime)
    custom_crypt = db.Column(db.Boolean)
    id_u = db.Column(db.String(64), index = True, unique = True)
    pub_date = db.Column(db.DateTime)
    
    
    
    
    def __init__(self, secret_f, author, email=None, custom_key = None):
        if custom_key:
            self.custom_crypt = True
            cryptor = AESCipher(custom_key)
        else:
            self.custom_crypt = False
            cryptor = AESCipher(config.SECRET_KEY)
        self.secret_f = cryptor.encrypt(secret_f)
        self.author = author
        self.email = email
        self.viewed = None        
        self.id_u = uuid.uuid4().urn[9:]
        self.pub_date = datetime.utcnow()
        
        
    def __repr__(self):
        return self.id
    
    
