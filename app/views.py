#!/usr/bin/env python
# -*- coding: utf-8 -*-




from app import app, db
from flask import render_template, flash, redirect, session, request, abort, url_for, g
from forms import RequestSecret, DecryptSecret, RequestSecret_no_login
from flask.ext.login import login_required, login_user, logout_user, current_user
from datetime import datetime, timedelta
import requests
import re
from models import User, SecretInst
from app.cryptograph import AESCipher
from config import SECRET_KEY, POSTS_PER_PAGE
from emails import notification
from app import babel
from config import LANGUAGES
from flask.ext.babel import gettext as _

@app.before_request
def get_current_user():
    g.user = None
    email = session.get('email')
    if email is not None:
        g.user = email





@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
def index():
    if current_user.is_authenticated:
        form = RequestSecret()
    else:
        form = RequestSecret_no_login()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            post = SecretInst(secret_f = unicode(form.secret.data).encode("utf8"),
                author = current_user,
                email = form.email.data,
                custom_key = unicode(form.passphrase.data).encode("utf8"))
            notification(form.email.data, request.host_url+'private/'+post.id_u)
        else:
            anonymous = User.query.filter_by(email = 'anonymous@anonymous.ru').first() 
            post = SecretInst(secret_f = unicode(form.secret.data).encode("utf8"),
                author = anonymous,
                custom_key = unicode(form.passphrase.data).encode("utf8"))
        db.session.add(post)
        db.session.commit()            
        return render_template('private.html',
            link = request.host_url+'private/'+post.id_u)
    else:
        return render_template('index.html',
            title = 'Request a secret',
            form = form)
        
        
@app.route('/_auth/login', methods=['GET', 'POST'])
def login_handler():
    resp = requests.post(app.config['PERSONA_VERIFIER'], data={
        'assertion': request.form['assertion'],
        'audience': request.host_url,
    }, verify=True)
    if resp.ok:
        verification_data = resp.json()
        if verification_data['status'] == 'okay':
            if not re.match(r'^.*@gmail.com$', verification_data['email']):
                flash(_('You have to use email only in @dtln.ru'))
                redirect(url_for('index'))
            session['email'] = verification_data['email']
            user = User.query.filter_by(email = session['email']).first()
            if user is None:
                user = User(email = session['email'])
                db.session.add(user)
                db.session.commit()
            login_user(user)
            return 'OK'

    abort(400)


@app.route('/_auth/logout', methods=['POST'])
@login_required
def logout_handler():
    logout_user()
    session.clear()
    return 'OK'
     

@app.route('/user/<int:page>')
@login_required
def user(page=1):
        user = User.query.filter_by(id = current_user.id).first()
        posts = SecretInst.query.filter_by(user_id = current_user.id).paginate(page, POSTS_PER_PAGE, False)
        return render_template('user.html',
            user = user,
            posts = posts)




@app.route('/private/<id_u>', methods = ['GET', 'POST'])
def private(id_u):
    post = SecretInst.query.filter_by(id_u = id_u).first()
    if post:
        if post.viewed == None:
            if post.custom_crypt:
                form = DecryptSecret()
                if form.validate_on_submit():
                    cryptor = AESCipher(unicode(form.passphrase.data).encode("utf8"))
                    try:
                        secret = cryptor.decrypt(post.secret_f).decode("utf8")   
                    except:
                        abort(500)
                    post.viewed = datetime.utcnow()
                    db.session.commit()
                    return render_template('private_open.html',secret = secret, title = 'View a secret')
                else:   
                    return render_template('private_openf.html',
                        title = 'View a secret',
                        form = form)
            else:
                cryptor = AESCipher(unicode(SECRET_KEY).encode("utf8"))
                secret = cryptor.decrypt(post.secret_f).decode("utf8")
                post.viewed = datetime.utcnow()
                db.session.commit()
                return render_template('private_open.html',secret = secret, title = 'View a secret')
        else:
            return abort(404)
    else:
        return abort(404)

@app.route('/clean')
def clean():
    period = datetime.now() - timedelta(days=3)
    res = SecretInst.query.filter(SecretInst.pub_date <= period)
    for inst in res:
        db.session.delete(inst)
        db.session.commit()
    return 'clean'




@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())