#!/usr/bin/env python
# -*- coding: utf-8 -*-



from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import DataRequired, Email
from flask.ext.login import current_user
from flask.ext.babel import gettext as _
from flask.ext.babel import lazy_gettext 


class RequestSecret(Form):
    secret = TextAreaField('secret', validators = [DataRequired(lazy_gettext("Secret content required"))])
    passphrase = TextField('passphrase', validators = [])
    email = TextField('secret', validators = [Email(lazy_gettext("Please enter valid email"))])
    
class RequestSecret_no_login(Form):
    secret = TextAreaField('secret', validators = [DataRequired(lazy_gettext("Secret content required"))])
    passphrase = TextField('passphrase', validators = [])

class DecryptSecret(Form):
    passphrase = TextField('passphrase', validators = [DataRequired(lazy_gettext("Passphrase required"))])