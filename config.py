#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

CSRF_ENABLED = True
SECRET_KEY = 'Enter your secret'

#Cryptography settings

BLOCK_SIZE = 32


PERSONA_JS='https://login.persona.org/include.js'
PERSONA_VERIFIER='https://verifier.login.persona.org/verify'

# mail server settings
MAIL_SERVER = 'localhost'
MAIL_PORT = 25
MAIL_USE_TLS = False
MAIL_USE_SSL = False
MAIL_USERNAME = None
MAIL_PASSWORD = None

# administrator list
ADMINS = ['admin@oneclick.ru', 'oneclick@oneclick.ru']

# pagination
POSTS_PER_PAGE = 5

# available languages
LANGUAGES = {
    'en': 'English',
    'ru': 'Русский'
}
